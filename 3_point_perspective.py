#3 point perspective
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import collections  as mc

def get_ind_under_point(xyt, event, epsilon = 0.05):
    xt, yt = xyt[:, 0], xyt[:, 1]
    d = np.hypot(xt - event.xdata, yt - event.ydata)
    indseq, = np.nonzero(d == d.min())
    ind = indseq[0]
    if d[ind] >= epsilon:
        ind = None
    return ind

def key_press_callback(event):
    '''
    i/I: insert/copy
    d: delete
    +/-: increase/decrease influence of one/all viewpoints
    e: make viewpoint distance unit-length
    r/R: rotate Viewpoints
    t/T: rotate individual
    '''
    if event.inaxes is None:
        return
    global P_D2, Van_Points
    _ind = get_ind_under_point(Van_Points,event)
    if _ind == None:
        if event.key == "i":
            P_R1.append(0)
            P_D2.append([.3,.3,.3])
            Van_Points = np.append(Van_Points,[[event.xdata,event.ydata]], axis = 0)
            update(_ind)
        return
    if event.key == "+":
        if _ind > 2:
            for i in range(3):
                P_D2[_ind-3][i] *= 1.01
        else:
            for i in range(len(P_D2)):
                P_D2[i][_ind] *= 1.01
    elif event.key == "-":
        if _ind >2:
            for i in range(3):
                P_D2[_ind-3][i] /= 1.01
        else:
            for i in range(len(P_D2)):
                P_D2 [i][_ind] /= 1.01
    elif event.key == "r" or event.key == "R":
        if event.key == "r":
            alpha = np.pi/50
        else:
            alpha = -np.pi/50
        Van_Points = rotate(Van_Points, alpha, _ind)
    elif event.key == "t" or event.key == "T":
        if event.key == "t":
            alpha = 1
        else:
            alpha = -1
        if _ind > 2:
            P_R1[_ind-3] = (P_R1[_ind-3]+alpha)%360
    elif event.key == "e":
        if _ind > 2:
            for i in range(3):
                van = Van_Points[i] - Van_Points[_ind]
                Van_Points[i] =van/np.linalg.norm(van)+Van_Points[_ind]
        else:
            van = Van_Points[_ind] - Van_Points[_ind]
            Van_Points[_ind] = van/np.linalg.norm(van)+Van_Points[_ind]
    elif event.key == "i":
        if _ind > 2:
            P_R1.append(P_R1[_ind-3]+0)
            P_D2.append([P_D2[_ind-3][0],P_D2[_ind-3][1],P_D2[_ind-3][2]])
        else: 
            P_R1.append(0)
            P_D2.append([.3,.3,.3])
        Van_Points = np.append(Van_Points,[[event.xdata,event.ydata]], axis = 0)
    elif event.key == "I":
        if _ind > 2:
            P_R1.append(P_R1[_ind-3])
            P_D2.append(P_D2[_ind-3])
        else:
            P_R1.append(0)
            P_D2.append([.3,.3,.3])
        Van_Points = np.append(Van_Points,[[event.xdata,event.ydata]], axis = 0)
    elif event.key == "d":
        if _ind > 2:
            P_D2.pop(_ind-3)
            P_R1.pop(_ind-3)
            Van_Points = np.delete(Van_Points, _ind, axis = 0)
    else: 
        return
    if _ind > 2:
        update(_ind)
    else:
        update()

def button_press_callback(event):
    global _ind
    if event.inaxes is None:
        return
    if event.button != 1:
        return
    _ind = get_ind_under_point(Van_Points,event)

def button_release_callback(event):
    global _ind
    if event.button != 1:
        return
    _ind = None

def motion_notify_callback(event):
    global _ind
    if _ind is None:
        return
    if event.inaxes is None:
        return
    if event.button != 1:
        return
    if event.xdata != None and event.ydata != None:
        Van_Points[_ind,0], Van_Points[_ind,1] = float(event.xdata), float(event.ydata)
        update()
    
def Schnittpunkt(P1, D1, P2, D2) -> np.ndarray:
    sol = np.zeros(2, dtype=float)
    c = P2-P1
    if D1[0] == 0:
        sol[1] = -c[0]/D2[0]
        sol[0] = (c[1]+D2[1]*sol[1])/D1[1]
    elif D1[1] == 0:
        sol[1] = -c[1]/D2[1]
        sol[0] = (c[0]+D2[0]*sol[1])/D1[0]
    else:
        sol[1] = (c[1]/D1[1]-c[0]/D1[0])/(D2[0]/D1[0]-D2[1]/D1[1])
        sol[0] = (c[1]+D2[1]*sol[1])/D1[1]
    sol1 = P1 + D1*sol
    sol2 = P2 + D2*sol
    sol[0], sol[1] = sol1[0],sol2[1]
    return sol

#Mid_Point = np.array([0,0.5])
Van_Points = np.array([[0,-0.5],[0.5,0.5],[-0.5,0.5],[0,0]], dtype= float)
P_R1 = [0]
P_D2 = [[.3,.3,.3]]
_ind = None

def rotate(Points:np.ndarray, alpha:float, _ind:int) -> np.ndarray:
    sin_a = np.sin(alpha)
    cos_a = np.cos(alpha)
    Van = np.zeros_like(Points)
    Van[3:] = Points[3:]
    Van1 =  Points[:3,0] - Points[_ind,0]
    Van2 =  Points[:3,1] - Points[_ind,1]
    Van[:3,0], Van[:3,1] = Points[_ind,0]+Van1*cos_a-Van2*sin_a, Points[_ind,1]+Van1*sin_a+Van2*cos_a
    return Van

def update(_ind = None):
    lines = []

    if True:#_ind == None:
        for_start = 0
        for_end = len(P_D2)
    else:
        for_start = _ind-3
        for_end = _ind-2
    for ind_ in range(for_start, for_end):
        Van = rotate(Van_Points, np.deg2rad(P_R1[ind_]), ind_+3)
        Draw_Points = np.zeros((7,2),dtype=float)
        Mid_Point = Van_Points[ind_+3]
        Draw_Points[0] = Mid_Point
        for i in range(3):
            line = Van[i] - Mid_Point
            #line = line/np.linalg.norm(line)
            End = Mid_Point+line*P_D2[ind_][i]
            Draw_Points[i+1] = End
            lines.append([(Mid_Point[0], Mid_Point[1]), (End[0], End[1])])
        for j in range(1,4):
            P1 = Draw_Points[(j)%3+1]
            P2 = Draw_Points[(j+1)%3+1]
            D1 = Van[(j+1)%3]-P1
            D2 = Van[(j)%3]-P2
            End = Schnittpunkt(P1, D1, P2, D2)
            Draw_Points[j+3] = End
            lines.append([(P1[0],P1[1]),(End[0],End[1])])
            lines.append([(P2[0],P2[1]),(End[0],End[1])])
        for k in range(7,8):
            P1 = Draw_Points[(k)%3+4]
            P2 = Draw_Points[(k+1)%3+4]
            D1 = Van[(k)%3]-P1
            D2 = Van[(k+1)%3]-P2
            End = Schnittpunkt(P1, D1, P2, D2)
            lines.append([(P1[0],P1[1]), (End[0],End[1])])
            lines.append([(P2[0],P2[1]), (End[0],End[1])])
            lines.append([(End[0],End[1]), (Draw_Points[(k+2)%3+4][0],Draw_Points[(k+2)%3+4][1])])
    lc.set_segments(lines)
    points.set_offsets(np.c_[Van_Points[:,0],Van_Points[:,1]])
    fig.canvas.draw()


fig, ax1 = plt.subplots()

lines = []
lc = mc.LineCollection(lines, linewidths=1, color = 'black')
lc.set_segments(lines)
ax1.add_collection(lc)

#ax1.scatter(Draw_Points[:,0],Draw_Points[:,1])
points = ax1.scatter(Van_Points[:,0],Van_Points[:,1],c="r")

fig.canvas.draw()
update()

ax1.set_xlim(-1,1)
ax1.set_ylim(-1,1)

fig.canvas.mpl_connect('key_press_event', key_press_callback)
fig.canvas.mpl_connect('button_press_event', button_press_callback)
fig.canvas.mpl_connect('button_release_event', button_release_callback)
fig.canvas.mpl_connect('motion_notify_event', motion_notify_callback)

plt.show()
